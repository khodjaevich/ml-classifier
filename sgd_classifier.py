from sklearn.linear_model import SGDClassifier
from sklearn.metrics import accuracy_score
from sklearn import preprocessing
from data import get_classes, get_data
from sklearn.model_selection import train_test_split

classes = get_classes()
data = get_data()

eightyPercent = int(len(data) * 0.8)

x_train = data[:eightyPercent]
x_test = data[eightyPercent:]

y_train = classes[:eightyPercent]
y_test = classes[eightyPercent:]

# Normal
clf = SGDClassifier()
clf.fit(x_train, y_train)
predicted = clf.predict(x_test)
print "Normal", accuracy_score(y_test, predicted)

# Normal + Scaling

train_scaled = preprocessing.scale(x_train)
test_scaled = preprocessing.scale(x_test)

clfScaled = SGDClassifier()
clfScaled.fit(train_scaled, y_train)
predictedScaled = clfScaled.predict(test_scaled)

print "Scaled:", accuracy_score(y_test, predictedScaled), '\n'

# Random

x_train, x_test, y_train, y_test = train_test_split(data, classes, train_size=0.8, random_state=len(data))

clf = SGDClassifier(loss="hinge")  # putting 10 and 1000 returns more accurate results.
clf.fit(x_train, y_train)
predicted = clf.predict(x_test)
print "Random", accuracy_score(y_test, predicted)

# Random + Scaling
train_scaled = preprocessing.scale(x_train)
test_scaled = preprocessing.scale(x_test)

clfScaled = SGDClassifier(loss="hinge")
clfScaled.fit(train_scaled, y_train)
predictedScaled = clfScaled.predict(test_scaled)
print "Random scaled:", accuracy_score(y_test, predictedScaled), '\n'

# Normal 0.896551724138
# Scaled: 0.655172413793
# Random 0.827586206897
# Random scaled: 0.758620689655


# Without Stars
# Random

without_star_data = get_data()

# Remove stars
for row in without_star_data:
    del row[int(len(row)/2)]
    del row[0]

x_train, x_test, y_train, y_test = train_test_split(
    without_star_data, classes, train_size=0.8, random_state=len(without_star_data))

clf = SGDClassifier()  # putting 10 and 1000 returns more accurate results.
clf.fit(x_train, y_train)
predicted = clf.predict(x_test)
print "Random (Removed Stars)", accuracy_score(y_test, predicted)

# Random + Scaling
train_scaled = preprocessing.scale(x_train)
test_scaled = preprocessing.scale(x_test)

clfScaled = SGDClassifier()
clfScaled.fit(train_scaled, y_train)
predictedScaled = clfScaled.predict(test_scaled)
print "Random scaled (Removed Stars)", accuracy_score(y_test, predictedScaled), '\n'



