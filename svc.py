from sklearn.svm import SVC
from sklearn.metrics import accuracy_score
from sklearn import preprocessing
from data import get_classes, get_data
from sklearn.model_selection import train_test_split
from random import *
import pprint
from scipy import mean

classes = get_classes()
data = get_data()

eightyPercent = int(len(data) * 0.8)

x_train = data[:eightyPercent]
x_test = data[eightyPercent:]

y_train = classes[:eightyPercent]
y_test = classes[eightyPercent:]

# Normal RBF
clf = SVC(C=5.0, kernel='rbf')
clf.fit(x_train, y_train)
predicted = clf.predict(x_test)
print "Normal RBF", accuracy_score(y_test, predicted)

# Normal POLY
clf = SVC(C=5.0, kernel='poly')
clf.fit(x_train, y_train)
predicted = clf.predict(x_test)
print "Normal POLY", accuracy_score(y_test, predicted)

# Normal LINEAR
clf = SVC(C=5.0, kernel='linear')
clf.fit(x_train, y_train)
predicted = clf.predict(x_test)
print "Normal LINEAR", accuracy_score(y_test, predicted)


# Normal SIGMOID
clf = SVC(C=5.0, kernel='sigmoid')
clf.fit(x_train, y_train)
predicted = clf.predict(x_test)
print "Normal SIGMOID", accuracy_score(y_test, predicted)

# Normal + Scaling

train_scaled = preprocessing.scale(x_train)
test_scaled = preprocessing.scale(x_test)

clfScaled = SVC(C=1000.0)
clfScaled.fit(train_scaled, y_train)
predictedScaled = clfScaled.predict(test_scaled)

print "Scaled:", accuracy_score(y_test, predictedScaled), '\n'

# Random
dictionary = [0,0,0,0]
dictionary[0] = []
dictionary[1] = []
dictionary[2] = []
dictionary[3] = []
for x in range(0,20):
    x_train, x_test, y_train, y_test = train_test_split(data, classes, train_size=0.8,
                                                        random_state=randint(1, len(data)))

    # Random RBF
    clf = SVC(C=5.0, kernel='rbf')
    clf.fit(x_train, y_train)
    predicted = clf.predict(x_test)
    rbfScore = round(accuracy_score(y_test, predicted),2)
    dictionary[0].append(rbfScore)
    print "Random RBF", rbfScore

    # Normal POLY
    clf = SVC(C=5.0, kernel='poly')
    clf.fit(x_train, y_train)
    predicted = clf.predict(x_test)
    polyScore = round(accuracy_score(y_test,predicted), 2)
    dictionary[1].append(polyScore)
    print "Random POLY", polyScore

    # Normal LINEAR
    clf = SVC(C=5.0, kernel='linear')
    clf.fit(x_train, y_train)
    predicted = clf.predict(x_test)
    linearScore = round(accuracy_score(y_test, predicted), 2)
    dictionary[2].append(linearScore)
    print "Random LINEAR", linearScore

    # Random SIGMOID
    clf = SVC(C=5.0, kernel='sigmoid')
    clf.fit(x_train, y_train)
    predicted = clf.predict(x_test)
    sigmoidScore = round(accuracy_score(y_test, predicted), 2)
    dictionary[3].append(sigmoidScore)
    print "Random SIGMOID", sigmoidScore


for values in dictionary:
    print values

print "Average"
for values in dictionary:
    print mean(values)

# Random + Scaling
train_scaled = preprocessing.scale(x_train)
test_scaled = preprocessing.scale(x_test)

clfScaled = SVC(C=1000.0)
clfScaled.fit(train_scaled, y_train)
predictedScaled = clfScaled.predict(test_scaled)
print "Random scaled:", accuracy_score(y_test, predictedScaled), '\n'


# Without Stars
# Random

without_star_data = get_data()

# Remove stars
for row in without_star_data:
    del row[int(len(row)/2)]
    del row[0]

x_train, x_test, y_train, y_test = train_test_split(
    without_star_data, classes, train_size=0.8, random_state=len(without_star_data))

clf = SVC(C=1.0)  # putting 10 and 1000 returns more accurate results.
clf.fit(x_train, y_train)
predicted = clf.predict(x_test)
print "Random (Removed Stars)", accuracy_score(y_test, predicted)

# Random + Scaling
train_scaled = preprocessing.scale(x_train)
test_scaled = preprocessing.scale(x_test)

clfScaled = SVC(C=1000.0)
clfScaled.fit(train_scaled, y_train)
predictedScaled = clfScaled.predict(test_scaled)
print "Random scaled (Removed Stars)", accuracy_score(y_test, predictedScaled), '\n'



